package lt.vilniusquiz.app;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.plus.PlusShare;

public class GoogleShareFragment extends Fragment {
    protected Activity activity;
    protected View view;
    protected String type, text, contentUrl;

    View.OnClickListener shareGClick =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // Launch the Google+ share dialog with attribution to your app.
            Intent shareIntent = new PlusShare.Builder(activity)
                    .setType(type)
                    .setText(text)
                    .setContentUrl(Uri.parse(contentUrl))
                    .getIntent();

            startActivityForResult(shareIntent, 0);
        }
    };

    public GoogleShareFragment() {
        type = "text/plain";
        text = "";
        contentUrl = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_google_share, container, false);
        view.setVisibility(View.INVISIBLE);
        activity = getActivity();
        ImageButton shareButton = (ImageButton) view.findViewById(R.id.share_button);
        shareButton.setOnClickListener(shareGClick);
        shareButton.performClick();
        return view;
    }

    public void setShareType(String type){
        this.type = type;
    }

    public void setShareText(String text){
        this.text = text;
    }

    public void setContentUrl(String contentUrl){
        this.contentUrl = contentUrl;
    }

}
