package lt.vilniusquiz.tinderLike.utils;

public interface OnCardDimissedListener {
	void onYes();
    void onNo();
}
