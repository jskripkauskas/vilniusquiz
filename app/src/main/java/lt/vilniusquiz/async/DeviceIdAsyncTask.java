package lt.vilniusquiz.async;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class DeviceIdAsyncTask extends AsyncTask<String, Void, String> {
    private final static String SOURCE_URL = "http://insanitydev.com/vilnius_quiz/add_notif.php";
    private String email = null;
    private String regId = null;

    public DeviceIdAsyncTask(String email, String regId){
        this.email = email;
        this.regId = regId;
    }
    @Override
    protected String doInBackground(String... params) {
        Log.d("DeviceIdAsyncTask", "Sending RegId");
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(SOURCE_URL);
            List nameValuePairs = new ArrayList();
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("reg_id", regId));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            return new BasicResponseHandler().handleResponse(response);
        } catch (Exception ex) {
            Log.e("SendRegId", ex.toString());
        }
        return null;
    }
}