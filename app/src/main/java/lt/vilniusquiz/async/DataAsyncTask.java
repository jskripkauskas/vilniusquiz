package lt.vilniusquiz.async;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


import android.os.AsyncTask;
import android.util.Log;

public class DataAsyncTask extends AsyncTask<String, Void, JSONObject> {
	private static String SOURCE_URL = "http://insanitydev.com/vilnius_quiz/get_questions.php";
	public DataAsyncResponse delegate = null;
    private String email = null;
    private int difficulty = 1;

    public DataAsyncTask(String email, int difficulty) {
        this.email = email;
        this.difficulty = difficulty;
    }

    @Override
	protected JSONObject doInBackground(String... params) {
		Log.d("DataAsyncTask", "Download data started");
		URL url;
        HttpURLConnection connection = null;
		try{
			url = new URL(SOURCE_URL);
			connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            List<NameValuePair> params2 = new ArrayList<NameValuePair>();
            params2.add(new BasicNameValuePair("email", email));
            params2.add(new BasicNameValuePair("difficulty", String.valueOf(difficulty)));

            OutputStream os = connection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params2));
            writer.flush();
            writer.close();
            os.close();
            connection.connect();
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
		    StringBuilder responseStrBuilder = new StringBuilder();

		    String inputStr;
		    while ((inputStr = streamReader.readLine()) != null){
		        responseStrBuilder.append(inputStr);
		    }
            Log.d("response", responseStrBuilder.toString());
		    return new JSONObject(responseStrBuilder.toString());
		} catch (Exception e){
			Log.d("CONTENT", e.getLocalizedMessage());
		} finally {
			if (connection != null){
				connection.disconnect();
			}
		}
		return null;
	}
	@Override
	protected void onPostExecute(JSONObject result) {
		delegate.processFinish(result);
	}
    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
