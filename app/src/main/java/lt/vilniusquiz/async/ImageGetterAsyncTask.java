package lt.vilniusquiz.async;


import java.io.InputStream;
import java.net.URL;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

public class ImageGetterAsyncTask  extends AsyncTask<String, Void, Drawable> {
	public ImageAsyncResponse delegate = null;
    private String id = null;
	private String name = null;
	private String address = null;
    private String latitude = null;
    private String longitude = null;
	private String url = null;

	private int result;
	public ImageGetterAsyncTask(String id, String name, String address, String latitude, String longitude, String url, int result) {
		this.url = url;
		this.name = name;
		this.address = address;
		this.result = result;
        this.id = id;
	}

	@Override
	protected Drawable doInBackground(String... params) {
		// TODO Auto-generated method stub
		try {
	        InputStream is = (InputStream) new URL(url).getContent();
	        Drawable d = Drawable.createFromStream(is, "src name");
	        return d;
	    } catch (Exception e) {
	        Log.d("LoadImageFromWebOperate", e.getLocalizedMessage());
	        return null;
	    }
	}
	@Override
	protected void onPostExecute(Drawable resultImage) {
		delegate.processImageFinish(id, name, address, latitude, longitude, resultImage, result);
	}

}
